import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItinerarioComponent } from './itinerario/itinerario.component';
import { MenuComponent } from './menu/menu.component';

const routes: Routes = [
  {path: 'itinerario/:id', component: ItinerarioComponent},
  {path: 'home', component: MenuComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
