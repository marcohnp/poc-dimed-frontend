import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LinhasOnibusComponent } from './linhas-onibus/linhas-onibus.component';
import { HttpClientModule } from '@angular/common/http';
import { LinhasLotacaoComponent } from './linhas-lotacao/linhas-lotacao.component';

import { ItinerarioComponent } from './itinerario/itinerario.component';
import { MenuComponent } from './menu/menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { BuscaLinhaOnibusComponent } from './busca-linha-onibus/busca-linha-onibus.component';
import { BuscaLinhaLotacaoComponent } from './busca-linha-lotacao/busca-linha-lotacao.component';


@NgModule({
  declarations: [
    AppComponent,
    LinhasOnibusComponent,
    LinhasLotacaoComponent,
    ItinerarioComponent,
    MenuComponent,
    HeaderComponent,
    BuscaLinhaOnibusComponent,
    BuscaLinhaLotacaoComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [BuscaLinhaOnibusComponent, BuscaLinhaLotacaoComponent],
  exports: [ MenuComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }

