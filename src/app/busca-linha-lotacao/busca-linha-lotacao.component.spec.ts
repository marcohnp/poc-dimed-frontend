import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscaLinhaLotacaoComponent } from './busca-linha-lotacao.component';

describe('BuscaLinhaLotacaoComponent', () => {
  let component: BuscaLinhaLotacaoComponent;
  let fixture: ComponentFixture<BuscaLinhaLotacaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscaLinhaLotacaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscaLinhaLotacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
