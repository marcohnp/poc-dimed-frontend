import { Component, OnInit } from '@angular/core';
import { TransporteService } from '../transporte.service';
import { LinhaLotacao } from '../models/linhalotacao.model';

export let buscaLinhasLotacao: LinhaLotacao[] = [] ;

@Component({
  selector: 'app-busca-linha-lotacao',
  templateUrl: './busca-linha-lotacao.component.html',
  styleUrls: ['./busca-linha-lotacao.component.css']
})
export class BuscaLinhaLotacaoComponent implements OnInit {

  linhLotacao = {} as LinhaLotacao;
  linhasDeLotacao: LinhaLotacao[] = [];

  constructor(
    private service: TransporteService
    ) { this.ngOnInit }

  ngOnInit(): void {
    this.service.getLinhasLotacao()
    .subscribe((linhasDeLotacao: LinhaLotacao[]) => {
      this.linhasDeLotacao = linhasDeLotacao;
      buscaLinhasLotacao = this.linhasDeLotacao;
    } );
  }

}

