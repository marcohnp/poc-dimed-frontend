import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscaLinhaOnibusComponent } from './busca-linha-onibus.component';

describe('BuscaLinhaOnibusComponent', () => {
  let component: BuscaLinhaOnibusComponent;
  let fixture: ComponentFixture<BuscaLinhaOnibusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscaLinhaOnibusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscaLinhaOnibusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
