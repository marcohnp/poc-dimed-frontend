import { Component, OnInit } from '@angular/core';
import { TransporteService } from '../transporte.service';
import { LinhaOnibus } from '../models/linhaonibus.model';

export let buscaLinhasOnibus: LinhaOnibus[] = [] ;

@Component({
  selector: 'app-busca-linha-onibus',
  templateUrl: './busca-linha-onibus.component.html',
  styleUrls: ['./busca-linha-onibus.component.css'],
  providers: [ TransporteService ]
})
export class BuscaLinhaOnibusComponent implements OnInit {

  linhaOnibus = {} as LinhaOnibus;
  linhasDeOnibus: LinhaOnibus[] = [];

  constructor(
    private service: TransporteService
    ) { this.ngOnInit }

  ngOnInit(): void {
    this.service.getLinhasOnibus()
    .subscribe((linhasDeOnibus: LinhaOnibus[]) => {
      this.linhasDeOnibus = linhasDeOnibus;
      buscaLinhasOnibus = this.linhasDeOnibus;
    } );
  }

}
