import {Injectable} from '@angular/core';

import {BehaviorSubject, Observable, of, Subject} from 'rxjs';

import { LinhaLotacao } from './models/linhalotacao.model'
import {DecimalPipe} from '@angular/common';
import {debounceTime, delay, switchMap, tap} from 'rxjs/operators';

import { buscaLinhasLotacao } from './busca-linha-lotacao/busca-linha-lotacao.component'

interface SearchResult {
  linhas: LinhaLotacao[];
  total: number;
}

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
}

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

function matches(linhas: LinhaLotacao, term: string) {
  return linhas.nome.toLowerCase().includes(term.toLowerCase())
}

@Injectable({providedIn: 'root'})
export class BuscaLotacaoService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _linhas$ = new BehaviorSubject<LinhaLotacao[]>([]);
  private _total$ = new BehaviorSubject<number>(0);

  linhasDeOninus: LinhaLotacao[] = [];

  private _state: State = {
    page: 1,
    pageSize: 20,
    searchTerm: '',
  };

  constructor(private pipe: DecimalPipe) {
    this._search$.pipe(
      tap(() => this._loading$.next(true)),
      debounceTime(200),
      switchMap(() => this._search()),
      delay(200),
      tap(() => this._loading$.next(false))
    ).subscribe(result => {
      this._linhas$.next(result.linhas);
      this._total$.next(result.total);
    });

    this._search$.next();
  }

  get linhas$() { return this._linhas$.asObservable(); }
  get total$() { return this._total$.asObservable(); }
  get loading$() { return this._loading$.asObservable(); }
  get page() { return this._state.page; }
  get pageSize() { return this._state.pageSize; }
  get searchTerm() { return this._state.searchTerm; }

  set page(page: number) { this._set({page}); }
  set pageSize(pageSize: number) { this._set({pageSize}); }
  set searchTerm(searchTerm: string) { this._set({searchTerm}); }

  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }

  private _search(): Observable<SearchResult> {
    const {pageSize, page, searchTerm} = this._state;

    // 1. sort
    let linhas = buscaLinhasLotacao;

    // 2. filter
    linhas= linhas.filter(linhas => matches(linhas, searchTerm));
    const total = linhas.length;

    // 3. paginate
    linhas = linhas.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
    return of({linhas, total});
  }
}
