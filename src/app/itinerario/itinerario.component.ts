import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Coordenadas } from '../models/coordenadas.model';
import { Itinerario } from '../models/itinerario.model';
import { TransporteService } from '../transporte.service';

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css'],
  providers: [ TransporteService]
})
export class ItinerarioComponent implements OnInit {

  id: string = "";
  itinerario = {} as Itinerario;
  coordenadas: Array<Coordenadas> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute, 
    private service: TransporteService
    ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    this.service.getItinerario(this.id)
    .subscribe((itinerarioResponse: Itinerario) => {
      console.log(itinerarioResponse); 
      this.itinerario.idlinha = itinerarioResponse.idlinha;
      this.itinerario.codigo = itinerarioResponse.codigo;
      this.itinerario.nome = itinerarioResponse.nome;    
    })

    this.service.getCoordenadas(this.id)
    .then((response: any) => {
      for(let i = 0; i < Object.keys(response).length - 3; i++){
        this.coordenadas.push(response[i]);
        
      }
      this.itinerario.coordenadas = this.coordenadas;
    })

  }

  goToGoogleMaps(lat: number, lng: number){
    window.open(`https://www.google.com/maps/?q=${lat},${lng}`, '_blank');
  }

  onTop(){
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  }

}
