import { DecimalPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { BuscaLotacaoService } from '../buscaLotacao.service';
import { LinhaLotacao } from '../models/linhalotacao.model';
import { TransporteService } from '../transporte.service';
import { BuscaLinhaLotacaoComponent } from '../busca-linha-lotacao/busca-linha-lotacao.component'

@Component({
  selector: 'app-linhas-lotacao',
  templateUrl: './linhas-lotacao.component.html',
  styleUrls: ['./linhas-lotacao.component.css'],
  providers: [ TransporteService, DecimalPipe, BuscaLotacaoService ]
})
export class LinhasLotacaoComponent implements OnInit {

  linhaLotacao = {} as LinhaLotacao;
  linhasDeLotacao: LinhaLotacao[] = [];

  linhas$: Observable<LinhaLotacao[]>;
  total$: Observable<number>

  constructor(
    private router: Router,
    private route: ActivatedRoute, 
    private service: TransporteService,
    public serviceBusca: BuscaLotacaoService,
    private busca: BuscaLinhaLotacaoComponent
    ) {
      this.linhas$ = serviceBusca.linhas$;
      this.total$ = serviceBusca.total$;
      this.busca.ngOnInit();
    }
    ngOnInit(): void {
      this.service.getLinhasLotacao()
      .subscribe((linhasDeLotacao: LinhaLotacao[]) => {
        this.linhasDeLotacao = linhasDeLotacao;
      } );
    }

    callItinerario(id: number){
      this.router.navigateByUrl('/itinerario/' + id)
    }

}
