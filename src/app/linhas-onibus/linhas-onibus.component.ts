import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransporteService } from '../transporte.service';
import { LinhaOnibus } from '../models/linhaonibus.model';
import { Observable } from 'rxjs';
import { DecimalPipe } from '@angular/common';;
import { BuscaOnibusService } from '../buscaOnibus.service';
import { BuscaLinhaOnibusComponent } from '../busca-linha-onibus/busca-linha-onibus.component'

@Component({
  selector: 'app-linhas-onibus',
  templateUrl: './linhas-onibus.component.html',
  styleUrls: ['./linhas-onibus.component.css'],
  providers: [ TransporteService, DecimalPipe, BuscaOnibusService ]
})
export class LinhasOnibusComponent implements OnInit {

  linhaOnibus = {} as LinhaOnibus;
  linhasDeOnibus: LinhaOnibus[] = [];

  linhas$: Observable<LinhaOnibus[]>;
  total$: Observable<number>

  constructor(
    private router: Router,
    private route: ActivatedRoute, 
    private service: TransporteService,
    public serviceBusca: BuscaOnibusService,
    private busca: BuscaLinhaOnibusComponent
    ) {
      this.linhas$ = serviceBusca.linhas$;
      this.total$ = serviceBusca.total$;
      this.busca.ngOnInit();
    }
     

  ngOnInit(): void {
    this.service.getLinhasOnibus()
    .subscribe((linhasDeOnibus: LinhaOnibus[]) => {
      this.linhasDeOnibus = linhasDeOnibus;
    } );
  }

  callItinerario(id: string){
    this.router.navigateByUrl('/itinerario/' + id)
  }

}
