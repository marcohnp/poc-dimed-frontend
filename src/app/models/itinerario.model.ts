import { Coordenadas } from "./coordenadas.model";

export interface Itinerario {
    idlinha: string;
    codigo: string;
    nome: string;
    coordenadas: Array<Coordenadas>;
}