import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { LinhaOnibus } from './models/linhaonibus.model';
import { LinhaLotacao } from './models/linhalotacao.model';
import { Itinerario } from './models/itinerario.model';
import { Observable, throwError } from "rxjs";
import { catchError, tap } from 'rxjs/operators'


@Injectable()
export class TransporteService {

    private url_api = 'http://www.poatransporte.com.br/php/facades/process.php';

    constructor(private http: HttpClient) {
    }

    public getLinhasOnibus(): Observable<LinhaOnibus[]> {
        return this.http.get<LinhaOnibus[]>(`${this.url_api}?a=nc&p=%&t=o`).pipe(
            tap(dados => console.log('Retornando todas Linhas de Onibus'),catchError(this.handleError))
          );
     }

     public getLinhasLotacao(): Observable<LinhaLotacao[]> {
        return this.http.get<LinhaLotacao[]>(`${this.url_api}?a=nc&p=%&t=l`).pipe(
            tap(dados => console.log('Retornando todas Linhas de Lotação'),catchError(this.handleError))
          );
     }

     public getItinerario(id: string): Observable<Itinerario> {
        return this.http.get<Itinerario>(`${this.url_api}?a=il&p=`+id).pipe(
            tap(dados => console.log('Retornando Itinerario'),catchError(this.handleError))
          );
     }

     public getCoordenadas(id: string): Promise<number> {
      return this.http.get(`${this.url_api}?a=il&p=`+id)
          .toPromise()
          .then((resposta: any) => {
              return resposta;
          
          })  
      }

     public handleError() {
        console.log("Erro encontrado na aplicação.")
        return (erro: HttpErrorResponse) => {
          let msg = '';
          if (erro.error instanceof ErrorEvent) {
            //falha no clado cliente
            console.log(erro.error.message)
            msg = `Falha no cliente: ${erro.error.message}`;
          } else {
            //falha no lado servidor
            console.log(erro.message)
            msg = `Falha no servidor: ${erro.status}, ${erro.statusText}, ${erro.message}`;
          }
          console.log("Erro => " + msg);
          return throwError(msg);
        }
      }
}